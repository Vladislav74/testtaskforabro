//
//  ValutaViewCell.swift
//  testTaskForAbro
//
//  Created by Владислав on 13.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import UIKit

class ValutaViewCell: UITableViewCell {

    @IBOutlet weak var valutaName: UILabel!
    @IBOutlet weak var valutaPrice: UILabel!
    
}
