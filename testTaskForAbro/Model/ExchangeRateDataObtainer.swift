//
//  ExchangeRateDataObtainer.swift
//  testTaskForAbro
//
//  Created by Владислав on 13.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
import Foundation

class ExchangeRateDataObtainer {
    
    static let notificationDataWasReloaded = "dataWasReloaded"
    static let notificationDataWasNotReceived = "dataWasNotReceived"
    
    private let url = NSURL(string: "http://api.fixer.io/latest?base=RUB")
    
    var valutaData = [Valuta]()
    
    func reloadVulutaData() {
       obtainDataForValuta()
    }
    
    private func obtainDataForValuta() {
        
       let request = NSURLRequest(URL: self.url!)
       let session = NSURLSession.sharedSession()
       let task = session.dataTaskWithRequest(request) {
          (data, response, error) -> Void in
             if error == nil {
                if let jsonData = data {
                   do {
                      let json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! [String:AnyObject]
                        
                      let rates = json["rates"]
                      let valutaNames = rates!.allKeys as! [String]
                    
                      self.valutaData.removeAll()
                      for name in valutaNames {
                       
                         let price = 1.0 / (rates![name] as! NSNumber).floatValue
                        
                         let priceFormat = (price >= 0.01) ? "%.2f" : "%.3f"
                        
                         let newValuta = Valuta()
                         newValuta.valutaName = name
                         newValuta.valutaPrice = String(format: priceFormat, price)
                         self.valutaData.append(newValuta)
                      }
                      dispatch_async(dispatch_get_main_queue()) {
                         NSNotificationCenter.defaultCenter().postNotificationName(ExchangeRateDataObtainer.notificationDataWasReloaded, object: nil)
                      }
                    
                   } catch {
                      self.someError()
                   }
                    
                } else {
                   self.someError()
                }
                
             } else {
                self.someError()
             }
       }
       task.resume()
    }
    
    private func someError() {
       dispatch_async(dispatch_get_main_queue()) {
          NSNotificationCenter.defaultCenter().postNotificationName(ExchangeRateDataObtainer.notificationDataWasNotReceived, object: nil)
       }
    }
}