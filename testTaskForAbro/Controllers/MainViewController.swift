//
//  MainViewController.swift
//  testTaskForAbro
//
//  Created by Владислав on 13.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var valutaTable: UITableView!
    
    let erDataObtainer = ExchangeRateDataObtainer()
    
    @IBAction func refreshButtonAction(sender: UIBarButtonItem) {
       erDataObtainer.reloadVulutaData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dataWasReloaded), name: ExchangeRateDataObtainer.notificationDataWasReloaded, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dataWasNotReceived), name: ExchangeRateDataObtainer.notificationDataWasNotReceived, object: nil)

        self.valutaTable.delegate = self
        self.valutaTable.dataSource = self

        let bundle = NSBundle(forClass: self.dynamicType)
        let cellNib = UINib(nibName: "ValutaViewCell", bundle: bundle)
        self.valutaTable.registerNib(cellNib, forCellReuseIdentifier: "ValutaViewCell")
    
        self.navigationBar.topItem?.title = "Курс валют"
        
        erDataObtainer.reloadVulutaData()
    }
    
    func dataWasReloaded() {
       self.valutaTable.reloadData()
    }
    
    func dataWasNotReceived() {
       let alert = UIAlertController(title: "Ошибка", message: "Не удалось получить данные", preferredStyle: .Alert)
       let action = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
       alert.addAction(action)
       presentViewController(alert, animated: true, completion: nil)
    }
    
    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return erDataObtainer.valutaData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellId = "ValutaViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! ValutaViewCell
        
        let valuta = erDataObtainer.valutaData[indexPath.row]
        
        cell.valutaName.text = valuta.valutaName
        cell.valutaPrice.text = valuta.valutaPrice + " руб"
        
        return cell
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
